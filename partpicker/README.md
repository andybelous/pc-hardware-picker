This PC configurator will allow users to create custom mainstream PC builds, calculate the power consumption, compatibility of all selected parts, suggest the right choice for newbies.
The main feature is allowing advanced users to add all pc parts by themself, this includes adding new images, description, price and everything you need.

This file is part of PC Hardware Picker.
    PC Hardware Picker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PC Hardware Picker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PC Hardware Picker.  If not, see <https://www.gnu.org/licenses/>.