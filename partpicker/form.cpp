#include "form.h"
#include "ui_form.h"

form::form(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::form)
{
    ui->setupUi(this);
    QFrame::setFrameShape( QFrame::Box);
     ui->picture_label->setStyleSheet("border: 1px solid grey");
}

form::~form()
{
    delete ui;
}
void form::item_set(item* itm)
{
        ui->picture_label->setPixmap(itm->getpicture());
        ui->header->setText(QString::fromStdString(itm->getname()));
        ui->description->setText(QString::fromStdString(itm->getdescription()));
        ui->pricelabel->setText("Price:"+QString::number(itm->getprice())+"$");
        current_itm=itm;
}


void form::on_chooseButton_clicked()
{

    connect(this,SIGNAL(item_chosen(item*)),this->window()->parent(),SLOT(on_item_set(item*)));
    emit item_chosen(current_itm);
}
