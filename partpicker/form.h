#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <QFrame>
#include "item.h"
namespace Ui {
class form;
}

class form : public QFrame
{
    Q_OBJECT

public:
    explicit form(QWidget *parent = nullptr);
    void item_set(item*);
    ~form();

signals:
    void item_chosen(item*);

private slots:


    void on_chooseButton_clicked();

private:
    Ui::form *ui;
    item* current_itm=nullptr;

};

#endif // FORM_H
