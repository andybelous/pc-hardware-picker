#include "item.h"
#include <QtXml>
#include <QDebug>

item::item()
{
}
item::item(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string a_picpath):id(a_id),name(a_name),price(a_price),
    description(a_description),pic(a_pic),picpath(a_picpath){};
void item::save_to_file()
{
}
void item::set_id(int a)
{
    id=a;
}
void item::setname(std::string a)
{
    name=a;
}
void item::set_price(int a)
{
    price=a;
}
void item::setdescriprion(std::string a)
{
    description=a;
}
int item::get_id() const
{
    return id;
}
std::string item::getname() const
{
    return name;
}
int item::getprice() const
{
    return price;
}

std::string item::getdescription() const
{
    return description;
}
const QPixmap& item::getpicture() const
{
    return pic;
}
string item::getpicpath() const
{
    return picpath;
}

processor::processor(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string a_picpath,const int a_socket,const int a_tdp):
    item(a_id, a_name, a_price, a_description,a_pic,a_picpath),socket(a_socket),tdp(a_tdp){};
void processor::setsocket(int a)
{
    socket=a;
}
void processor::settdp(int a)
{
    tdp=a;
}
int processor::getsocket() const
{
    return socket;
}
int processor::gettdp() const
{
    return tdp;
}
void processor::save_to_file()
{
    QFile file("xml/"+QString::fromStdString(name)+".xml");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Open the file for writing failed";
    }
    else
    {
        QXmlStreamWriter stream(&file);
        stream.setAutoFormatting(true);
        stream.writeStartDocument();
        stream.writeStartElement("hdd");
        stream.writeTextElement("id",QString::number(id));
        stream.writeTextElement("name",QString::fromStdString(name));
        stream.writeTextElement("price",QString::number(price));
        stream.writeTextElement("description",QString::fromStdString(description));
        stream.writeTextElement("picpath",QString::fromStdString(picpath));
        stream.writeTextElement("socket",QString::number(socket));
        stream.writeTextElement("tdp",QString::number(tdp));
        stream.writeEndElement();
        stream.writeEndDocument();
        file.close();
        qDebug() << "Writing is done";
    }
}


motherboard::motherboard(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string picpath,const int a_socket):
    item(a_id, a_name, a_price, a_description,a_pic,picpath),socket(a_socket){}
void motherboard::setsocket(int a)
{
    socket=a;
}
int motherboard::getsocket() const
{
   return socket;
}
void motherboard::save_to_file()
{   
    QFile file("xml/"+QString::fromStdString(name)+".xml");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Open the file for writing failed";
    }
    else
    {
        QXmlStreamWriter stream(&file);
        stream.setAutoFormatting(true);
        stream.writeStartDocument();
        stream.writeStartElement("hdd");
        stream.writeTextElement("id",QString::number(id));
        stream.writeTextElement("name",QString::fromStdString(name));
        stream.writeTextElement("price",QString::number(price));
        stream.writeTextElement("description",QString::fromStdString(description));
        stream.writeTextElement("picpath",QString::fromStdString(picpath));
        stream.writeTextElement("socket",QString::number(socket));
        stream.writeEndElement();
        stream.writeEndDocument();
        file.close();
        qDebug() << "Writing is done";
    }
}

ram::ram(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string picpath,const int a_capacity):
    item(a_id, a_name, a_price, a_description,a_pic,picpath),capacity(a_capacity){}

void ram::setcapacity(int a)
{
    capacity=a;
}
int ram::getcapacity() const
{
    return capacity;
}
void ram::save_to_file()
{   
    QFile file("xml/"+QString::fromStdString(name)+".xml");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Open the file for writing failed";
    }
    else
    {
        QXmlStreamWriter stream(&file);
        stream.setAutoFormatting(true);
        stream.writeStartDocument();
        stream.writeStartElement("hdd");
        stream.writeTextElement("id",QString::number(id));
        stream.writeTextElement("name",QString::fromStdString(name));
        stream.writeTextElement("price",QString::number(price));
        stream.writeTextElement("description",QString::fromStdString(description));
        stream.writeTextElement("picpath",QString::fromStdString(picpath));
        stream.writeTextElement("capacity",QString::number(capacity));
        stream.writeEndElement();
        stream.writeEndDocument();
        file.close();
        qDebug() << "Writing is done";
    }
}



powersupply::powersupply(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string picpath,const int a_power):
    item(a_id, a_name, a_price, a_description,a_pic,picpath),power(a_power){}

void powersupply::setpower(int a)
{
    power=a;
}
int powersupply::getpower() const
{
    return power;
}
void powersupply::save_to_file()
{
    QFile file("xml/"+QString::fromStdString(name)+".xml");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Open the file for writing failed";
    }
    else
    {
        QXmlStreamWriter stream(&file);
        stream.setAutoFormatting(true);
        stream.writeStartDocument();
        stream.writeStartElement("hdd");
        stream.writeTextElement("id",QString::number(id));
        stream.writeTextElement("name",QString::fromStdString(name));
        stream.writeTextElement("price",QString::number(price));
        stream.writeTextElement("description",QString::fromStdString(description));
        stream.writeTextElement("picpath",QString::fromStdString(picpath));
        stream.writeTextElement("power",QString::number(power));
        stream.writeEndElement();
        stream.writeEndDocument();
        file.close();
        qDebug() << "Writing is done";
    }
}



gpu::gpu(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string picpath,const int a_tdp):
    item(a_id, a_name, a_price, a_description,a_pic,picpath),tdp(a_tdp){}

void gpu::settdp(int a)
{
    tdp=a;
}
int gpu::gettdp() const
{
    return tdp;
}
void gpu::save_to_file()
{   
    QFile file("xml/"+QString::fromStdString(name)+".xml");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Open the file for writing failed";
    }
    else
    {
        QXmlStreamWriter stream(&file);
        stream.setAutoFormatting(true);
        stream.writeStartDocument();
        stream.writeStartElement("hdd");
        stream.writeTextElement("id",QString::number(id));
        stream.writeTextElement("name",QString::fromStdString(name));
        stream.writeTextElement("price",QString::number(price));
        stream.writeTextElement("description",QString::fromStdString(description));
        stream.writeTextElement("picpath",QString::fromStdString(picpath));
        stream.writeTextElement("tdp",QString::number(tdp));
        stream.writeEndElement();
        stream.writeEndDocument();
        file.close();
        qDebug() << "Writing is done";
    }
}


hdd::hdd(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string picpath,const int a_capacity):item(a_id, a_name, a_price, a_description,a_pic,picpath),
    capacity(a_capacity){}
void hdd::setcapacity(int a)
{
    capacity=a;
}
int hdd::getcapacity() const
{
    return capacity;
}
void hdd::save_to_file()
{
    QFile file("xml/"+QString::fromStdString(name)+".xml");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Open the file for writing failed";
    }
    else
    {
        QXmlStreamWriter stream(&file);
        stream.setAutoFormatting(true);
        stream.writeStartDocument();
        stream.writeStartElement("hdd");
        stream.writeTextElement("id",QString::number(id));
        stream.writeTextElement("name",QString::fromStdString(name));
        stream.writeTextElement("price",QString::number(price));
        stream.writeTextElement("description",QString::fromStdString(description));
        stream.writeTextElement("picpath",QString::fromStdString(picpath));
        stream.writeTextElement("capacity",QString::number(capacity));
        stream.writeEndElement();
        stream.writeEndDocument();
        file.close();
        qDebug() << "Writing is done";
    }
}



