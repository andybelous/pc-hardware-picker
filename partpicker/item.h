#ifndef ITEM_H
#define ITEM_H
#include <string>
#include <QPixmap>
#include <fstream>
using namespace std;
class item{
protected:
    int id;
    std::string name;
    int price;
    std::string description;
    QPixmap pic;
    string picpath;
public:
    item();
    virtual ~item(){}
    item(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string a_picpath);
    virtual void save_to_file();
    void set_id(const int a);
    void setname(const std::string a);
    void set_price(const int a);
    void setdescriprion(const std::string a);
    int get_id() const;
    std::string getname() const;
    int getprice() const;
    std::string getdescription() const;
    const QPixmap& getpicture() const;
    string getpicpath() const;
};
class processor: public item
{
protected:
    int socket;
    int tdp;
public:
    processor(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string a_picpath,const int a_socket,const int a_tdp);
    void setsocket(int a);
    void settdp(int a);
    int getsocket() const;
    int gettdp() const;
    void save_to_file();
};
class motherboard :public item
{
protected:
    int socket;
public:
    motherboard(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string picpath,const int a_socket);
    void setsocket(int a);
    int getsocket() const;
    void save_to_file();
};
class ram :public item
{
protected:
    int capacity;
public:
    ram(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string picpath,const int a_capacity);
    void setcapacity(int a);
    int getcapacity() const;
    void save_to_file();
};
class powersupply :public item
{
protected:
    int power;
public:
    powersupply(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string picpath,const int a_power);
    void setpower(int a);
    int getpower() const;
    void save_to_file();
};
class gpu :public item
{
protected:
    int tdp;
public:
    gpu(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string picpath,const int a_tdp);
    void settdp(int a);
    int gettdp() const;
    void save_to_file();
};
class hdd :public item
{
protected:
    int capacity;
public:
    hdd(const int a_id,const std::string a_name,const int a_price,const std::string a_description,const QPixmap& a_pic,const string picpath,const int a_capacity);
    void setcapacity(int a);
    int getcapacity() const;
    void save_to_file();
};
#endif // ITEM_H
