#include "itemadder.h"
#include "ui_itemadder.h"
#include "itemview.h"
#include <fstream>
#include <string>
#include <cstdio>
#include <QtXml>
#include <QDebug>
#include <algorithm>
using namespace std;
itemadder::itemadder(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::itemadder)
{
    ui->setupUi(this);
    layout = new QVBoxLayout(ui->scrollArea);
    itemview* itemviewer=new itemview();
    itemviews.push_back(itemviewer);
    inner=new QFrame(ui->scrollArea);
    inner->setLayout(layout);
    ui->scrollArea->setWidget(inner);
    load_items();
}

itemadder::~itemadder()
{
    for(auto it=itemviews.begin();it!=itemviews.end();it++)
        delete *it;
    for(auto it=vec.begin();it!=vec.end();it++)
        delete *it;
    delete ui;
    delete layout;
    delete inner;
}


void itemadder::on_addButton_clicked()
{
    itemview* itemviewer=new itemview();
    itemviews.push_back(itemviewer);
    inner->layout()->addWidget(itemviewer);
}
void itemadder::load_items()
{
    QString cutpath;
    QDirIterator it(QDir::currentPath()+"/xml", QStringList() << "*.xml", QDir::Files, QDirIterator::Subdirectories);

    while (it.hasNext()) {
        item* itm;
        QString data=it.next();
        qDebug()<<data;
        QFile file(data);
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            qDebug() << "Open the file for writing failed";
        }
        QXmlStreamReader* xmlReader=new QXmlStreamReader(&file);
        string id,name,price,description,picpath,socket,tdp,capacity,power;
        QPixmap pic;
        bool iscomplete=false;
        while(!xmlReader->atEnd() && !xmlReader->hasError()) {
                QXmlStreamReader::TokenType token = xmlReader->readNext();
                if(token == QXmlStreamReader::StartDocument) {
                        continue;
                }
                if(token == QXmlStreamReader::StartElement) {

                        if(xmlReader->name() == "id") {
                            id=xmlReader->readElementText().toUtf8().constData();
                        }

                        if(id=="1")
                        {
                            if(xmlReader->name() == "name") {
                                name=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "price") {
                                price=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "description") {
                                description=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "picpath") {
                                picpath=xmlReader->readElementText().toUtf8().constData();
                            }

                            pic.load(QString::fromStdString(picpath).remove("/"));
                            if(xmlReader->name() == "socket") {
                                socket=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "tdp") {
                                tdp=xmlReader->readElementText().toUtf8().constData();
                                iscomplete=true;
                            }
                            if(iscomplete)
                            {
                            itm=new processor(1,name,atoi(price.c_str()),description,pic,picpath,atoi(socket.c_str()),atoi(tdp.c_str()));
                            vec.push_back(itm);
                            iscomplete=false;
                            }
                        }
                        else if(id=="2")
                        {
                            if(xmlReader->name() == "name") {
                                name=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "price") {
                                price=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "description") {
                                description=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "picpath") {
                                picpath=xmlReader->readElementText().toUtf8().constData();
                            }
                            pic.load(QString::fromStdString(picpath).remove("/"));
                            if(xmlReader->name() == "socket") {
                                socket=xmlReader->readElementText().toUtf8().constData();
                                iscomplete=true;
                            }
                            if(iscomplete)
                            {
                            itm=new motherboard(2,name,atoi(price.c_str()),description,pic,picpath,atoi(socket.c_str()));
                            vec.push_back(itm);
                            iscomplete=false;
                            }
                        }
                        else if(id=="3")
                        {
                            if(xmlReader->name() == "name") {
                                name=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "price") {
                                price=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "description") {
                                description=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "picpath") {
                                picpath=xmlReader->readElementText().toUtf8().constData();
                            }
                            pic.load(QString::fromStdString(picpath).remove("/"));
                            if(xmlReader->name() == "capacity") {
                                capacity=xmlReader->readElementText().toUtf8().constData();
                                iscomplete=true;
                            }
                            if(iscomplete)
                            {
                            itm=new ram(3,name,atoi(price.c_str()),description,pic,picpath,atoi(capacity.c_str()));
                            vec.push_back(itm);
                            iscomplete=false;
                            }
                        }
                        else if(id=="4")
                        {
                            if(xmlReader->name() == "name") {
                                name=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "price") {
                                price=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "description") {
                                description=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "picpath") {
                                picpath=xmlReader->readElementText().toUtf8().constData();
                            }
                            pic.load(QString::fromStdString(picpath).remove("/"));
                            if(xmlReader->name() == "power") {
                                power=xmlReader->readElementText().toUtf8().constData();
                                iscomplete=true;
                            }
                            if(iscomplete)
                            {
                            itm=new powersupply(4,name,atoi(price.c_str()),description,pic,picpath,atoi(power.c_str()));
                            vec.push_back(itm);
                            iscomplete=false;
                            }
                        }
                        else if(id=="5")
                        {
                            if(xmlReader->name() == "name") {
                                name=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "price") {
                                price=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "description") {
                                description=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "picpath") {
                                picpath=xmlReader->readElementText().toUtf8().constData();
                            }
                            pic.load(QString::fromStdString(picpath).remove("/"));
                            if(xmlReader->name() == "tdp") {
                                tdp=xmlReader->readElementText().toUtf8().constData();
                                iscomplete=true;
                            }
                            if(iscomplete)
                            {
                            itm=new gpu(5,name,atoi(price.c_str()),description,pic,picpath,atoi(tdp.c_str()));
                            vec.push_back(itm);
                            iscomplete=false;
                            }
                        }
                        else if(id=="6")
                        {
                            if(xmlReader->name() == "name") {
                                name=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "price") {
                                price=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "description") {
                                description=xmlReader->readElementText().toUtf8().constData();
                            }
                            if(xmlReader->name() == "picpath") {
                                picpath=xmlReader->readElementText().toUtf8().constData();
                            pic.load(QString::fromStdString(picpath).remove("/"));
                            }

                            if(xmlReader->name() == "capacity") {
                                capacity=xmlReader->readElementText().toUtf8().constData();
                                iscomplete=true;
                            }
                            if(iscomplete)
                            {
                            itm=new hdd(6,name,atoi(price.c_str()),description,pic,picpath,atoi(capacity.c_str()));
                            qDebug()<<QString::fromStdString(name)<<QString::fromStdString(description);
                            vec.push_back(itm);
                            iscomplete=false;
                            }
                        }
                }
        }
        file.close();
        delete xmlReader;

    }
    display_items();
}
void itemadder::display_items()
{
    for(auto ptr=vec.begin();ptr<vec.end();ptr++)
    {
        itemview* itemviewer=new itemview();
        inner->layout()->addWidget(itemviewer);
        itemviewer->item_set(*ptr);
        qDebug()<<QString::fromStdString((*ptr)->getname());
    }

}

void itemadder::delete_item(itemview* const item,const std::string& name,int arg)
{
    std::pair<itemview*,string> p(item,name);
    if(arg==Qt::Checked)
    {
        delvec.push_back(p);
    }
    else if(arg==Qt::Unchecked)
    {
        auto position=std::find(delvec.begin(),delvec.end(),p);
        if (position != delvec.end())
            delvec.erase(position);
    }
}
void itemadder::on_deleteButton_clicked()
{
    if(!delvec.empty())
    {
        for(auto it=delvec.begin();it!=delvec.end();++it)
        {
        if((*it).second.length()==0)
        {
            this->layout->removeWidget((*it).first);
            delete (*it).first;
        }
        else if( remove( ("xml/"+(*it).second+".xml").c_str() ) != 0 )
        {
            perror( "Error deleting file" );
            this->layout->removeWidget((*it).first);
            delete (*it).first;
        }
        else
        {
            this->layout->removeWidget((*it).first);
            delete (*it).first;
        }
        }
        delvec.clear();
    }

}
