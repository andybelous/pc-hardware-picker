#ifndef ITEMADDER_H
#define ITEMADDER_H
#include <QDialog>
#include <QVBoxLayout>
#include <QFrame>
#include <QMessageBox>
#include <QDirIterator>
#include "item.h"
#include "itemview.h"
namespace Ui {
class itemadder;
}
class itemadder : public QDialog
{
    Q_OBJECT

public:
    explicit itemadder(QWidget *parent = nullptr);
    ~itemadder();
    void load_items();
    void display_items();
private slots:

    void on_addButton_clicked();


    void on_deleteButton_clicked();
    void delete_item(itemview* const,const std::string&,const int);

private:
    Ui::itemadder *ui;
    QVBoxLayout *layout;
    QFrame* inner;
    std::vector<item*> vec;
    vector<itemview*> itemviews;
    std::vector<std::pair<itemview*,string>> delvec;
};

#endif // ITEMADDER_H
