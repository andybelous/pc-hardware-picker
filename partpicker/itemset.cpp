#include "itemset.h"

itemset::itemset()
{

}
itemset::itemset(const itemset& x):iproc(x.iproc),imother(x.imother),iram(x.iram),
    isupply(x.isupply),igpu(x.igpu),ihdd(x.ihdd),price(x.price),
    powercons(x.powercons){}
itemset::itemset(processor* a_proc,motherboard* a_mother,ram* a_ram, powersupply* a_supply, gpu* a_gpu, hdd* a_hdd,int a_price, int a_powercons)
    : iproc(a_proc),imother(a_mother),iram(a_ram),isupply(a_supply),igpu(a_gpu),ihdd(a_hdd),
      price(a_price),powercons(a_powercons){};
void itemset::setprocessor(processor* a_proc)
{
    iproc=a_proc;
}
void itemset::setmotherboard(motherboard* a_mother)
{
    imother=a_mother;
}
void itemset::setram(ram* a_ram)
{
    iram=a_ram;
}
void itemset::setpowersupply(powersupply* a_supply)
{
    isupply=a_supply;
}
void itemset::setgpu(gpu* a_gpu)
{
    igpu=a_gpu;
}
void itemset::sethdd(hdd* a_hdd)
{
    ihdd=a_hdd;
}
void itemset::setprice(int a_price)
{
    price=a_price;
}
void itemset::setpowercons(int a_powercons)
{
    powercons=a_powercons;
}
processor* itemset::getprocessor() const
{
    return iproc;
}
motherboard* itemset::getmotherboard() const
{
    return imother;
}
ram* itemset::getram() const
{
    return iram;
}
powersupply* itemset::getpowersupply() const
{
    return isupply;
}
gpu* itemset::getgpu() const
{
    return igpu;
}
hdd* itemset::gethdd() const
{
    return ihdd;
}
int itemset::getprice() const
{
    return price;
}
int itemset::getpowercons() const
{
    return powercons;
}
void itemset::incprice(int xprice)
{
    price+=xprice;
}
void itemset::incpower(int xpower)
{
    powercons+=xpower;
}
itemset::~itemset()
{
    iproc=nullptr;
    imother=nullptr;
    iram=nullptr;
    isupply=nullptr;
    igpu=nullptr;
    ihdd=nullptr;
}
int itemset::iscomplete() const
{
    if(this->getprocessor()==nullptr||this->getmotherboard()==nullptr||
this->getram()==nullptr||this->getpowersupply()==nullptr||this->getgpu()==nullptr||this->gethdd()==nullptr)
        return 0;
    else return 1;
}
void itemset::clear()
{
    if(iproc!=nullptr)
    {
    iproc=nullptr;
    }
    if(imother!=nullptr)
    {
    imother=nullptr;
    }
    if(iram!=nullptr)
    {
    iram=nullptr;
    }
    if(isupply!=nullptr)
    {
    isupply=nullptr;
    }
    if(igpu!=nullptr)
    {
    igpu=nullptr;
    }
    price=0;
    powercons=0;
}
