#ifndef ITEMSET_H
#define ITEMSET_H
#include "item.h"

class itemset
{
protected:
    processor* iproc=nullptr;
    motherboard* imother=nullptr;
    ram* iram=nullptr;
    powersupply* isupply=nullptr;
    gpu* igpu=nullptr;
    hdd* ihdd=nullptr;
    int price=0;
    int powercons=0;
public:
    itemset();
    ~itemset();
    itemset(processor* a_proc,motherboard* a_mother,ram* a_ram, powersupply* a_supply, gpu* a_gpu, hdd* a_hdd,int a_price, int a_powercons);
    itemset(const itemset&);
    void setprocessor(processor* a_proc);
    void setmotherboard(motherboard* a_mother);
    void setram(ram* a_ram);
    void setpowersupply(powersupply* a_supply);
    void setgpu(gpu* a_gpu);
    void sethdd(hdd* a_hdd);
    void setprice(int a_price);
    void setpowercons(int a_powercons);
    void incprice(int);
    void incpower(int);
    void clear();

    processor* getprocessor() const;
    motherboard* getmotherboard() const;
    ram* getram() const;
    powersupply* getpowersupply() const;
    gpu* getgpu() const;
    hdd* gethdd() const;
    int getprice() const;
    int getpowercons() const;
    int iscomplete() const;

};

#endif // ITEMSET_H
