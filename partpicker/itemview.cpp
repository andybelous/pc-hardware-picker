#include "itemview.h"
#include "ui_itemview.h"
#include "item.h"
#include <cstdio>
itemview::itemview(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::itemview)
{
    ui->setupUi(this);
    QFrame::setFrameShape( QFrame::Box);
}

itemview::~itemview()
{
    delete ui;
}



void itemview::on_fileSelectButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,"Select image",QDir::currentPath(),"All files (*.*) ;; JPEG files (*jpg *.jpeg);;PNG files (*.png)");
    QString cutdir=fileName.mid(fileName.lastIndexOf("/"));
    img_path=cutdir;
    qDebug()<<cutdir;
    ui->image_view->setText("Image selected:"+img_path);
}


void itemview::on_save_Button_clicked()
{
    item* itm=nullptr;
    if(ui->typeBox->currentText()=="Processor")
    {
        std::string name=(ui->nameEdit->text()).toUtf8().constData();
        std::string price=(ui->priceEdit->text()).toUtf8().constData();
        std::string description=(ui->descriptionEdit->toPlainText()).toUtf8().constData();
        std::string socket=(ui->socketBox->currentText()).toUtf8().constData();
        std::string tdp=((ui->tdpEdit->text()).toUtf8().constData());
        QPixmap* picture=new QPixmap();
        picture->load(img_path);

        itm=new processor(1,name,atoi(price.c_str()),description,*picture,img_path.toUtf8().constData(),atoi(socket.c_str()),atoi(tdp.c_str()));
        qDebug()<<"id:"<<itm->get_id()<<"name:"<<QString::fromStdString(itm->getname())<<"Description:"<<QString::fromStdString(itm->getdescription());
        itm->save_to_file();
        delete picture;
    }
    else if(ui->typeBox->currentText()=="Motherboard")
    {
        std::string name=(ui->nameEdit->text()).toUtf8().constData();
        std::string price=(ui->priceEdit->text()).toUtf8().constData();
        std::string description=(ui->descriptionEdit->toPlainText()).toUtf8().constData();
        std::string socket=(ui->socketBox->currentText()).toUtf8().constData();
        QPixmap* picture=new QPixmap();
        picture->load(img_path);

        itm=new motherboard(2,name,atoi(price.c_str()),description,*picture,img_path.toUtf8().constData(),atoi(socket.c_str()));
        qDebug()<<"id:"<<itm->get_id()<<"name:"<<QString::fromStdString(itm->getname())<<"Description:"<<QString::fromStdString(itm->getdescription());
        itm->save_to_file();
        delete picture;
    }
    else if(ui->typeBox->currentText()=="Ram")
    {
        std::string name=(ui->nameEdit->text()).toUtf8().constData();
        std::string price=(ui->priceEdit->text()).toUtf8().constData();
        std::string description=(ui->descriptionEdit->toPlainText()).toUtf8().constData();
        std::string capacity=(ui->capacityEdit->text()).toUtf8().constData();
        QPixmap* picture=new QPixmap();
        picture->load(img_path);

        itm=new ram(3,name,atoi(price.c_str()),description,*picture,img_path.toUtf8().constData(),atoi(capacity.c_str()));
        qDebug()<<"id:"<<itm->get_id()<<"name:"<<QString::fromStdString(itm->getname())<<"Description:"<<QString::fromStdString(itm->getdescription());
        itm->save_to_file();
        delete picture;
    }
    else if(ui->typeBox->currentText()=="PowerSupply")
    {
        std::string name=(ui->nameEdit->text()).toUtf8().constData();
        std::string price=(ui->priceEdit->text()).toUtf8().constData();
        std::string description=(ui->descriptionEdit->toPlainText()).toUtf8().constData();
        std::string power=((ui->powerEdit->text()).toUtf8().constData());
        QPixmap* picture=new QPixmap();
        picture->load(img_path);

        itm=new powersupply(4,name,atoi(price.c_str()),description,*picture,img_path.toUtf8().constData(),atoi(power.c_str()));
        qDebug()<<"id:"<<itm->get_id()<<"name:"<<QString::fromStdString(itm->getname())<<"Description:"<<QString::fromStdString(itm->getdescription());
        itm->save_to_file();
        delete picture;
    }
    else if(ui->typeBox->currentText()=="Videocard")
    {
        std::string name=(ui->nameEdit->text()).toUtf8().constData();
        std::string price=(ui->priceEdit->text()).toUtf8().constData();
        std::string description=(ui->descriptionEdit->toPlainText()).toUtf8().constData();
        std::string tdp=((ui->tdpEdit->text()).toUtf8().constData());
        QPixmap* picture=new QPixmap();
        picture->load(img_path);

        itm=new gpu(5,name,atoi(price.c_str()),description,*picture,img_path.toUtf8().constData(),atoi(tdp.c_str()));
        qDebug()<<"id:"<<itm->get_id()<<"name:"<<QString::fromStdString(itm->getname())<<"Description:"<<QString::fromStdString(itm->getdescription());
        itm->save_to_file();
        delete picture;
    }
    else if(ui->typeBox->currentText()=="HDD"||ui->typeBox->currentText()=="SSD")
    {
        std::string name=(ui->nameEdit->text()).toUtf8().constData();
        std::string price=(ui->priceEdit->text()).toUtf8().constData();
        std::string description=(ui->descriptionEdit->toPlainText()).toUtf8().constData();
        std::string capacity=(ui->capacityEdit->text()).toUtf8().constData();
        QPixmap* picture=new QPixmap();
        picture->load(img_path);

        itm=new hdd(6,name,atoi(price.c_str()),description,*picture,img_path.toUtf8().constData(),atoi(capacity.c_str()));
        qDebug()<<"id:"<<itm->get_id()<<"name:"<<QString::fromStdString(itm->getname())<<"Description:"<<QString::fromStdString(itm->getdescription());
        itm->save_to_file();
        delete picture;
    }

    if(itm!=nullptr)
        delete itm;
}
void itemview::item_set(item* itm)
{


    char conv[30];
    ui->descriptionEdit->setText(QString::fromStdString(itm->getdescription()));
    ui->nameEdit->setText(QString::fromStdString(itm->getname()));
    ui->priceEdit->setText(QString::fromStdString(itoa(itm->getprice(),conv,10)));
    ui->image_view->setText("Image"+QString::fromStdString(itm->getpicpath()));
    if(processor* d=dynamic_cast<processor*>(itm))
    {
        ui->tdpEdit->setText(QString::number(d->gettdp()));
        ui->typeBox->setCurrentText("Processor");
    }
    else if(motherboard* d=dynamic_cast<motherboard*>(itm))
    {
        ui->typeBox->setCurrentText("Motherboard");
    }
    else if(ram* d=dynamic_cast<ram*>(itm))
    {
        ui->typeBox->setCurrentText("Ram");
    }
    else if(powersupply* d=dynamic_cast<powersupply*>(itm))
    {
        ui->typeBox->setCurrentText("PowerSupply");
        ui->powerEdit->setText(QString::number(d->getpower()));
    }
    else if(gpu* d=dynamic_cast<gpu*>(itm))
    {
        ui->typeBox->setCurrentText("GPU");
        ui->tdpEdit->setText(QString::number(d->gettdp()));
    }
    else if(hdd* d=dynamic_cast<hdd*>(itm))
    {
        ui->typeBox->setCurrentText("HDD");
        ui->capacityEdit->setText(QString::number(d->getcapacity()));
    }
}


void itemview::on_checkBox_stateChanged(int arg1)
{
    connect(this,SIGNAL(delete_item(itemview* const,const std::string&,const int)),this->window(),SLOT(delete_item(itemview* const,const std::string&,const int)));
    emit delete_item(this,(this->ui->nameEdit->text()).toUtf8().constData(),arg1);
}
