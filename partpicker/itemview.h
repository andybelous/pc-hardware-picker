#ifndef ITEMVIEW_H
#define ITEMVIEW_H

#include <QWidget>
#include <QFrame>
#include <QFileDialog>
#include <QDebug>
#include "item.h"
namespace Ui {
class itemview;
}

class itemview : public QFrame
{
    Q_OBJECT

public:
    explicit itemview(QWidget *parent = nullptr);
    ~itemview();
    void item_set(item*);
signals:
    void delete_item(itemview* const,const std::string&,const int);

private slots:

    void on_fileSelectButton_clicked();

    void on_save_Button_clicked();

    void on_checkBox_stateChanged(int arg1);

private:
    Ui::itemview *ui;
    QString img_path;
};

#endif // ITEMVIEW_H
