#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "logindialog.h"
#include "userview.h"
#include <QtDebug>
#include <QtXml>
#include <QDateTime>
#include <QInputDialog>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->cpulabel->setStyleSheet("border: 1px solid black");
    ui->motherlabel->setStyleSheet("border: 1px solid black");
    ui->ramlabel->setStyleSheet("border: 1px solid black");
    ui->powerlabel->setStyleSheet("border: 1px solid black");
    ui->gpulabel->setStyleSheet("border: 1px solid black");
    ui->hddlabel->setStyleSheet("border: 1px solid black");
    load_items();
    mainset=new itemset();

}

MainWindow::~MainWindow()
{
    delete mainset;
    for(auto it=vec.begin();it!=vec.end();++it)
    {
            delete *it;
    }
    delete ui;

}

void MainWindow::on_actionLogin_triggered()
{
    loginDialog* login=new loginDialog(this);
    login->show();
}


void MainWindow::on_processorButton_clicked()
{
    userview* procview=new userview(this);
    procview->show();
    procview->display_items(&vec,PROCESSOR);
}

void MainWindow::load_items()
{
    QString cutpath;
    QDirIterator it(QDir::currentPath()+"/xml", QStringList() << "*.xml", QDir::Files, QDirIterator::Subdirectories);

    while (it.hasNext()) {
    item* itm;
    QString data=it.next();
    qDebug()<<data;
    QFile file(data);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Open the file for writing failed";
    }
    QXmlStreamReader* xmlReader=new QXmlStreamReader(&file);
    string id,name,price,description,picpath,socket,tdp,capacity,power;
    QPixmap pic;
    bool iscomplete=false;
    while(!xmlReader->atEnd() && !xmlReader->hasError()) {
            QXmlStreamReader::TokenType token = xmlReader->readNext();
            if(token == QXmlStreamReader::StartDocument) {
                    continue;
            }
            if(token == QXmlStreamReader::StartElement) {

                    if(xmlReader->name() == "id") {
                        id=xmlReader->readElementText().toUtf8().constData();
                    }

                    if(id=="1")
                    {
                        if(xmlReader->name() == "name") {
                            name=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "price") {
                            price=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "description") {
                            description=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "picpath") {
                            picpath=xmlReader->readElementText().toUtf8().constData();
                        }

                        pic.load(QString::fromStdString(picpath).remove("/"));
                        if(xmlReader->name() == "socket") {
                            socket=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "tdp") {
                            tdp=xmlReader->readElementText().toUtf8().constData();
                            iscomplete=true;
                        }
                        if(iscomplete)
                        {
                        itm=new processor(1,name,atoi(price.c_str()),description,pic,picpath,atoi(socket.c_str()),atoi(tdp.c_str()));
                        vec.push_back(itm);
                        iscomplete=false;
                        }
                    }
                    else if(id=="2")
                    {
                        if(xmlReader->name() == "name") {
                            name=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "price") {
                            price=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "description") {
                            description=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "picpath") {
                            picpath=xmlReader->readElementText().toUtf8().constData();
                        }
                        pic.load(QString::fromStdString(picpath).remove("/"));
                        if(xmlReader->name() == "socket") {
                            socket=xmlReader->readElementText().toUtf8().constData();
                            iscomplete=true;
                        }
                        if(iscomplete)
                        {
                        itm=new motherboard(2,name,atoi(price.c_str()),description,pic,picpath,atoi(socket.c_str()));
                        vec.push_back(itm);
                        iscomplete=false;
                        }
                    }
                    else if(id=="3")
                    {
                        if(xmlReader->name() == "name") {
                            name=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "price") {
                            price=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "description") {
                            description=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "picpath") {
                            picpath=xmlReader->readElementText().toUtf8().constData();
                        }
                        pic.load(QString::fromStdString(picpath).remove("/"));
                        if(xmlReader->name() == "capacity") {
                            capacity=xmlReader->readElementText().toUtf8().constData();
                            iscomplete=true;
                        }
                        if(iscomplete)
                        {
                        itm=new ram(3,name,atoi(price.c_str()),description,pic,picpath,atoi(capacity.c_str()));
                        vec.push_back(itm);
                        iscomplete=false;
                        }
                    }
                    else if(id=="4")
                    {
                        if(xmlReader->name() == "name") {
                            name=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "price") {
                            price=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "description") {
                            description=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "picpath") {
                            picpath=xmlReader->readElementText().toUtf8().constData();
                        }
                        //QPixmap* pic=new QPixmap();
                        pic.load(QString::fromStdString(picpath).remove("/"));
                        if(xmlReader->name() == "power") {
                            power=xmlReader->readElementText().toUtf8().constData();
                            iscomplete=true;
                        }
                        if(iscomplete)
                        {
                        itm=new powersupply(4,name,atoi(price.c_str()),description,pic,picpath,atoi(power.c_str()));
                        vec.push_back(itm);
                        iscomplete=false;
                        }
                    }
                    else if(id=="5")
                    {
                        if(xmlReader->name() == "name") {
                            name=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "price") {
                            price=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "description") {
                            description=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "picpath") {
                            picpath=xmlReader->readElementText().toUtf8().constData();
                        }
                        //QPixmap* pic=new QPixmap();
                        pic.load(QString::fromStdString(picpath).remove("/"));
                        if(xmlReader->name() == "tdp") {
                            tdp=xmlReader->readElementText().toUtf8().constData();
                            iscomplete=true;
                        }
                        if(iscomplete)
                        {
                        itm=new gpu(5,name,atoi(price.c_str()),description,pic,picpath,atoi(tdp.c_str()));
                        vec.push_back(itm);
                        iscomplete=false;
                        }
                    }
                    else if(id=="6")
                    {
                        if(xmlReader->name() == "name") {
                            name=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "price") {
                            price=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "description") {
                            description=xmlReader->readElementText().toUtf8().constData();
                        }
                        if(xmlReader->name() == "picpath") {
                            picpath=xmlReader->readElementText().toUtf8().constData();
                        pic.load(QString::fromStdString(picpath).remove("/"));
                        }

                        if(xmlReader->name() == "capacity") {
                            capacity=xmlReader->readElementText().toUtf8().constData();
                            iscomplete=true;
                        }
                        if(iscomplete)
                        {
                        itm=new hdd(6,name,atoi(price.c_str()),description,pic,picpath,atoi(capacity.c_str()));
                        qDebug()<<QString::fromStdString(name)<<QString::fromStdString(description);
                        vec.push_back(itm);
                        iscomplete=false;
                        }
                    }
            }
    }
    file.close();
    delete xmlReader;

}

}

void MainWindow::on_motherboardButton_clicked()
{
    userview* procview=new userview(this);
    procview->show();
    procview->display_items(&vec,MOTHERBOARD);
}

void MainWindow::on_ramButton_clicked()
{
    userview* procview=new userview(this);
    procview->show();
    procview->display_items(&vec,RAM);
}

void MainWindow::on_powerButton_clicked()
{
    userview* procview=new userview(this);
    procview->show();
    procview->display_items(&vec,POWERSUPPLY);
}

void MainWindow::on_gpuButton_clicked()
{
    userview* procview=new userview(this);
    procview->show();
    procview->display_items(&vec,GPU);
}

void MainWindow::on_hddButton_clicked()
{
    userview* procview=new userview(this);
    procview->show();
    procview->display_items(&vec,HDD);
}
void MainWindow::on_item_set(item* itm)
{
    if(processor* d=dynamic_cast<processor*>(itm))
    {
        mainset->setprocessor(d);
        ui->cpulabel->setPixmap(d->getpicture());
        ui->cpuname_label->setText(QString::fromStdString(d->getname()));
        mainset->incprice(d->getprice());
        mainset->incpower(d->gettdp());
        refresh_stats();
        if(mainset->iscomplete())
            check_compability();
    }
    else if(motherboard* d=dynamic_cast<motherboard*>(itm))
    {
        mainset->setmotherboard(d);
        ui->motherlabel->setPixmap(d->getpicture());
        ui->mothername_label->setText(QString::fromStdString(d->getname()));
        mainset->incprice(d->getprice());
        mainset->incpower(10);
        refresh_stats();
        if(mainset->iscomplete())
            check_compability();

    }
    else if(ram* d=dynamic_cast<ram*>(itm))
    {
        mainset->setram(d);
        ui->ramlabel->setPixmap(d->getpicture());
        ui->ramname_label->setText(QString::fromStdString(d->getname()));
        mainset->incprice(d->getprice());
        mainset->incpower(5);
        refresh_stats();
        if(mainset->iscomplete())
            check_compability();

    }
    else if(powersupply* d=dynamic_cast<powersupply*>(itm))
    {
        mainset->setpowersupply(d);
        ui->powerlabel->setPixmap(d->getpicture());
        ui->powersupname_label->setText(QString::fromStdString(d->getname()));
        mainset->incprice(d->getprice());
        refresh_stats();
        if(mainset->iscomplete())
            check_compability();
    }
    else if(gpu* d=dynamic_cast<gpu*>(itm))
    {
        mainset->setgpu(d);
        ui->gpulabel->setPixmap(d->getpicture());
        ui->gpuname_label->setText(QString::fromStdString(d->getname()));
        mainset->incprice(d->getprice());
        mainset->incpower(d->gettdp());
        refresh_stats();
        if(mainset->iscomplete())
            check_compability();

    }
    else if(hdd* d=dynamic_cast<hdd*>(itm))
    {
        mainset->sethdd(d);
        ui->hddlabel->setPixmap(d->getpicture());
        ui->hddname_label->setText(QString::fromStdString(d->getname()));
        mainset->incprice(d->getprice());
        mainset->incpower(10);
        refresh_stats();
        if(mainset->iscomplete())
            check_compability();
    }
}
void MainWindow::check_compability()
{
    QMessageBox msg;
    if(mainset->getprocessor()==nullptr||mainset->getmotherboard()==nullptr||
mainset->getram()==nullptr||mainset->getpowersupply()==nullptr||mainset->getgpu()==nullptr||mainset->gethdd()==nullptr)
            msg.setText("Pick all parts of the set");
    else if(mainset->getprocessor()->getsocket()!=mainset->getmotherboard()->getsocket())
        msg.setText("Socket of processor and motherboard should be same");
    else if(mainset->getpowercons()>mainset->getpowersupply()->getpower())
        msg.setText("Power consuption exceeds power supply limit");
    else
    {
        msg.setText("Everything is good");
        ui->bulbButton->setIcon(QIcon(":/img/C:/Users/andyb/Desktop/bulb.png"));
    }
    msg.exec();
}

void MainWindow::on_bulbButton_clicked()
{
    check_compability();
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox msg(this);
    msg.setText("This Software is created by Andrew Bilous. If you want to contact me for cooperation, plz feel free write at andybelous@ukr.net");
    msg.setStandardButtons(QMessageBox::Ok);
    msg.exec();
}

void MainWindow::on_saveSetButton_clicked()
{
    if(!mainset->iscomplete())
    {
        QMessageBox msg(this);
        msg.setText("Set is not complete yet!");
        msg.setStandardButtons(QMessageBox::Ok);
        msg.exec();
    }
    else
    {
    bool ok;
    QString filename = QInputDialog::getText(this, tr("Save Partset"),
                                         tr("Enter the name of the file:"), QLineEdit::Normal,
                                         QDir::home().dirName(), &ok);

    if (ok && !filename.isEmpty())
    {
        QFile file(".\\savedata\\"+filename+".txt");
       if(!file.open(QIODevice::WriteOnly|QIODevice::Text))
           qDebug()<<"Error saving file";
        QTextStream out(&file);
        out<<"******Your PC Set******\n";
        out<<"CPU: "<<QString::fromStdString(mainset->getprocessor()->getname()+"\n");
        out<<"Motherboard: "<<QString::fromStdString(mainset->getmotherboard()->getname()+"\n");
        out<<"RAM: "<<QString::fromStdString(mainset->getram()->getname()+"\n");
        out<<"Power Supply: "<<QString::fromStdString(mainset->getpowersupply()->getname()+"\n");
        out<<"GPU: "<<QString::fromStdString(mainset->getgpu()->getname()+"\n");
        out<<"HDD/SSD: "<<QString::fromStdString(mainset->gethdd()->getname()+"\n");
        out<<"Total price: "<<mainset->getprice()<<" Power consumption: "<<mainset->getpowercons()<<"\n";
        file.close();
    }
    }

}

void MainWindow::on_clearButton_clicked()
{
    mainset->clear();
    ui->cpulabel->setPixmap(QPixmap());
    ui->cpulabel->setText("Choose CPU");
    ui->cpuname_label->setText("");
    ui->motherlabel->setPixmap(QPixmap());
    ui->motherlabel->setText("Choose motherboard");
    ui->mothername_label->setText("");
    ui->ramlabel->setPixmap(QPixmap());
    ui->ramlabel->setText("Choose RAM");
    ui->ramname_label->setText("");
    ui->powerlabel->setPixmap(QPixmap());
    ui->powerlabel->setText("Choose power supply");
    ui->powersupname_label->setText("");
    ui->gpulabel->setPixmap(QPixmap());
    ui->gpulabel->setText("Choose GPU");
    ui->gpuname_label->setText("");
    ui->hddlabel->setPixmap(QPixmap());
    ui->hddlabel->setText("Choose HDD/SSD");
    ui->hddname_label->setText("");
    refresh_stats();
}
void MainWindow::refresh_stats()
{
    ui->pricelabel->setText("Price:"+QString::number(mainset->getprice())+"$");
    ui->powercalc->setText("Power calculation:"+QString::number(mainset->getpowercons())+"W");
}

