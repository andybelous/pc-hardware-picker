#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <string>
#include <QMainWindow>
#include "item.h"
#include "itemset.h"
#define PROCESSOR 1
#define MOTHERBOARD 2
#define RAM 3
#define POWERSUPPLY 4
#define GPU 5
#define HDD 6
#define SSD 7
#define COOLER 8
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void load_items();
    void check_compability();
    void refresh_stats();
private slots:
    void on_actionLogin_triggered();


    void on_processorButton_clicked();

    void on_motherboardButton_clicked();

    void on_ramButton_clicked();

    void on_powerButton_clicked();

    void on_gpuButton_clicked();

    void on_hddButton_clicked();

    void on_item_set(item*);

    void on_bulbButton_clicked();

    void on_actionAbout_triggered();

    void on_saveSetButton_clicked();

    void on_clearButton_clicked();



private:
    Ui::MainWindow *ui;
    std::vector<item*> vec;
    itemset* mainset;
};

#endif // MAINWINDOW_H
