#-------------------------------------------------
#
# Project created by QtCreator 2018-09-29T14:00:02
#
#-------------------------------------------------

QT       += core gui
QT +=xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = partpicker
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    item.cpp \
    itemadder.cpp \
    logindialog.cpp \
    itemview.cpp \
    userview.cpp \
    form.cpp \
    itemset.cpp

HEADERS += \
        mainwindow.h \
    item.h \
    itemadder.h \
    logindialog.h \
    itemview.h \
    userview.h \
    form.h \
    itemset.h

FORMS += \
        mainwindow.ui \
    itemadder.ui \
    logindialog.ui \
    itemview.ui \
    userview.ui \
    form.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    rsc.qrc
win32:RC_ICONS += icon.ico

DISTFILES += \
    C:/Users/andyb/Desktop/icon.ico
