#include "userview.h"
#include "ui_userview.h"
#include "form.h"
userview::userview(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::userview)
{
    ui->setupUi(this);
    layout = new QVBoxLayout(ui->scrollArea);
    inner=new QFrame(ui->scrollArea);
    inner->setLayout(layout);
    ui->scrollArea->setWidget(inner);
}

userview::~userview()
{
    delete ui;
    delete layout;
    delete inner;
}

void userview::display_items(vector<item*>* item_vec,int type)
{
    vector<item*>::iterator ptr;
    for(ptr=item_vec->begin();ptr<item_vec->end();ptr++)
    {
        form* item_list=new form(this->parentWidget());

        if(processor* d=dynamic_cast<processor*>(*ptr))
        {
           if(type==PROCESSOR)
           {
            item_list->item_set(*ptr);
            inner->layout()->addWidget(item_list);
           }
        }
        else if(motherboard* d=dynamic_cast<motherboard*>(*ptr))
        {
            if(type==MOTHERBOARD)
            {
             item_list->item_set(*ptr);
             inner->layout()->addWidget(item_list);
            }
        }
        else if(ram* d=dynamic_cast<ram*>(*ptr))
        {
            if(type==RAM)
            {
             item_list->item_set(*ptr);
             inner->layout()->addWidget(item_list);
            }
        }
        else if(powersupply* d=dynamic_cast<powersupply*>(*ptr))
        {
            if(type==POWERSUPPLY)
            {
             item_list->item_set(*ptr);
             inner->layout()->addWidget(item_list);
            }
        }
        else if(gpu* d=dynamic_cast<gpu*>(*ptr))
        {
            if(type==GPU)
            {
             item_list->item_set(*ptr);
             inner->layout()->addWidget(item_list);
            }
        }
        else if(hdd* d=dynamic_cast<hdd*>(*ptr))
        {
            if(type==HDD)
            {
             item_list->item_set(*ptr);
             inner->layout()->addWidget(item_list);
            }
        }

    }
}
