#ifndef USERVIEW_H
#define USERVIEW_H

#include <QDialog>
#include <QDialog>
#include <QVBoxLayout>
#include <QFrame>
#include <QMessageBox>
#include <QDirIterator>
#include "item.h"
#include "mainwindow.h"
namespace Ui {
class userview;
}

class userview : public QDialog
{
    Q_OBJECT

public:
    explicit userview(QWidget *parent = nullptr);
    ~userview();
    QVBoxLayout *layout;
    QFrame* inner;
    void display_items(vector<item*>*, int type);
private:
    Ui::userview *ui;

};

#endif // USERVIEW_H
